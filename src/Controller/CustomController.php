<?php
namespace Drupal\vci\Controller;

use Drupal\vci\Service\GenericHelper;
use Drupal\Core\Controller\ControllerBase;
class CustomController extends ControllerBase {

  public function updateViewsCount() {
    /** @var GenericHelper $service */
    $service = \Drupal::service("vci.generic_helper");
    return $service->updateFinanceKPIView();
  }

}
