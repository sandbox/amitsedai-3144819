<?php

namespace Drupal\vci\Service;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\views\Views;
use Exception;

class GenericHelper {

  public function updateFinanceKPIView() {
    $query = \Drupal::entityQuery('block_content')
      ->condition('type', 'indicator');
    $indicator_type_block_id = $query->execute();
    foreach ($indicator_type_block_id as $id) {
      $entity = \Drupal::entityTypeManager()->getStorage('block_content')->load($id);
      if (!is_object($entity)) {
        return [];
      }
      $this->updateFinanceKPIs($entity);
    }
    return [
      '#type' => 'markup',
      '#markup' => 'Updated',
    ];
  }
  public function updateFinanceKPIs($blockEntity){
    $indicatorsEntity = $blockEntity->field_indicators->referencedEntities();
    $itemsPerPage = 5000;
    $this->updateKPIBlock($indicatorsEntity,$itemsPerPage);
  }

  public function updateKPIBlock($indicatorsEntity,$itemsPerPage) {
    foreach ($indicatorsEntity as $element) {
      $paragraph = Paragraph::load($element->id());
      $view_link = $paragraph->field_indicator_view_link->uri;
      $view_link = str_replace('internal:/','',$view_link);
      $url = \Drupal::service('path.validator')->getUrlIfValid($view_link);
      if ($url && $url->isRouted()) {
        $route_params = explode('.', $url->getRouteName());
        if ($route_params[0] === 'view' && $view = Views::getView($route_params[1])) {
          $view->setDisplay($route_params[2]);
          $options = $url->getOptions();
          $exposed_filters = $options["query"];
          if(isset($exposed_filters)){
            $view->setExposedInput($exposed_filters);
          }
          $view->setItemsPerPage($itemsPerPage);
          $view->build();
          $view->execute();
          $view_result = $view->result;
          $paragraph->set('field_indicator_help_text', count($view_result));
        }
      }
      $paragraph->save();
    }
  }
}
